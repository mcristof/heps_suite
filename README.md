# heps_suite

In this docker you can find:

1. ROOT
2. LHAPDF
3. POWHEG-BOX v2
4. Pythia 8.2
5. RIVET
6. Yoda
7. Delphes
8. RAVE

## Prerequisites
You should have Docker Engine installed on your machine. You can find [here](https://docs.docker.com/engine/install/) the instructions to install it on your machine.

## Get the docker image
Run the following command to let `docker` access `gitlab-registry.cern.ch`:
```bash
docker login gitlab-registry.cern.ch
```
Then, you can pull the docker image:
```bash
docker pull gitlab-registry.cern.ch/mcristof/heps_suite:latest
```

## Usage
If you set aliases in your .bashrc, you can run the tools from your shell. e.g:

```
alias DelphesPythia8='docker run -i  --rm  -u `id -u $USER`:`id -g`  -v $PWD:$PWD -w $PWD gitlab-registry.cern.ch/mcristof/heps_suite DelphesPythia8'
alias DelphesHepMC='docker run -i  --rm  -u `id -u $USER`:`id -g`  -v $PWD:$PWD -w $PWD gitlab-registry.cern.ch/mcristof/heps_suite DelphesHepMC'
```

In the example folder there is a delphes and a pythia8 card. After setting the alias you can try to run:

`DelphesPythia8 delphes_card_ATLAS.tcl ttbar.cmnd out.root`

and see if you obtain a non-empty out.root



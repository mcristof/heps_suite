#ifndef __FASTJET_NEWJETVARIABLES_HH__
#define __FASTJET_NEWJETVARIABLES_HH__
#include "fastjet/PseudoJet.hh"
#include <map>
#include <string>
#include <vector>
using namespace std;

FASTJET_BEGIN_NAMESPACE

vector<PseudoJet>boostToCenterOfMass(PseudoJet jet,vector<PseudoJet> constit_pseudojets);
float Angularity (PseudoJet &jet);
float PlanarFlow (PseudoJet &jet);
float KtDeltaR(PseudoJet &jet, double m_jetrad);
map<string, double> SphericityTensor(PseudoJet &jet);
double KtSplittingScale(PseudoJet &jet);
double ZCut( PseudoJet &jet);


FASTJET_END_NAMESPACE
#endif // ANGULARITY_H_INCLUDED_H_INCLUDED
